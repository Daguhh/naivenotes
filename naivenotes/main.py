#!/usr/bin/env python3

import sys, os
from itertools import islice

from PyQt5.QtWidgets import QApplication
from PyQt5.QtCore import Qt

from gui import MainWindow
from converter import Converter
import config as cf


class NaiveNoteApp(MainWindow):

    def __init__(self):

        make_configs()
        super().__init__()

        # Shortcut
        self.editor = self.note.editor
        self.preview = self.note.preview
        self.status = self.note.status

        self.S = self.files.view.selected
        self.view = self.files.view

        # Make connections
        self.connect_widgets()

        # Not supported yet
        self.status.propertiesBtn.setEnabled(False)

        # Set markdown render
        self.html_render = Converter()
        self.html_render.setrender("rst")
        self.editor.set_html_render(self.html_render)

        self.timer.start()
        self.update_files_list()


    def connect_widgets(self):

        self.preview.follow(self.editor)
        self.status.detachBtn.pressed.connect(self.note.dtach_editor)
        self.status.nameEdit.returnPressed.connect(self.update_file_name)
        self.status.toggleviewBtn.pressed.connect(self.toggle_view)
        self.status.nameEdit.sig.enterEditionEvent.connect(
            self.display_full_file_path
        )

        self.files.treeWdg.currentItemChanged.connect(self.load_file)
        self.files.listWdg.currentItemChanged.connect(self.load_file)
        self.files.createBtn.pressed.connect(self.create_new_file)
        self.files.delBtn.pressed.connect(self.delete_file)
        self.files.search.sig.enterEditionEvent.connect(self.files.toggle_search_mod)
        self.files.search.textChanged.connect(self.search_for_file)
        self.timer.timeout.connect(self.save_current_file)

    def update_files_list(self):

        self.view.clear_all()

        for path, _, files in islice(os.walk(cf.CONFIG_PATH), 1, None):
            folder = path.split("/")[-1]
            if folder == ".corbeille":
                continue
            self.view.addFolder(folder)
            for file in files:
                self.view.addFile(file, folder)

    def save_current_file(self):

        S = self.S

        if S.item != None and S.type == 'file':
            with open(S.path, "w") as f:
                f.write(self.editor.gettext())

    def search_for_file(self):

        keyword = self.files.search.text()
        self.files.search_file(keyword)

    def display_full_file_path(self):

        S = self.S
        self.status.setname(f"{S.folder}/{S.name}")

    def delete_file(self):

        S = self.S
        folder_path = f"{cf.CONFIG_PATH}/{S.folder}"

        os.rename(f"{folder_path}/{S.name}", f"{cf.CONFIG_PATH}/.corbeille/{S.name}")
        self.view.rmFile()

        if not os.listdir(folder_path):
            os.rmdir(folder_path)
            self.view.rmFolder(S.folder)

    def update_file_name(self):

        old_file_path = self.S.path
        old_folder_path = '/'.join(old_file_path.split('/')[:-1])
        old_folder = old_folder_path.split("/")[-1]
        new_folder, new_name = self.status.nameEdit.text().split("/")[-2:]
        new_file_path = f"{cf.CONFIG_PATH}/{new_folder}/{new_name}"

        self.S.name = new_name

        if old_folder != new_folder:
            os_mvfile(old_file_path, new_file_path)
            self.view.mvFile(new_name, new_folder)

            if not os.listdir(old_folder_path):
                os.rmdir(old_folder_path)
                self.view.rmFolder(old_folder)

        else:
            os.remove(old_file_path)

    def create_new_file(self):

        gen_name = gen_new_file_name()

        newfile_name = next(gen_name)
        newfile_folder = self.S.folder
        new_file_path = f"{cf.CONFIG_PATH}/{newfile_folder}/{newfile_name}"

        while True:
            if not os.path.isfile(new_file_path):
                with open(new_file_path, 'w') as f:
                    f.write('type here')
                break
            else:
                newfile_name = next(gen_name)
                new_file_path = f"{cf.CONFIG_PATH}/{newfile_folder}/{newfile_name}"
        self.view.addFile2CurFolder(newfile_name)

    def load_file(self):

        S = self.S

        if S.item != None and S.type == "file":

            ext = S.path.split('.')[-1]
            print(ext)
            self.html_render.setrender(ext)

            with open(S.path) as f:
                text = f.read()
            self.editor.setText(text)
            self.status.setname(S.name)

def gen_new_file_name():

    i = 0
    name = "new_file.md"
    while True:
        yield name
        i += 1
        name = f'new_file({i}).md'

def os_mvfile(old_file_path, new_file_path):

    folder = new_file_path.split('/')[-2]
    if not folder in os.listdir(cf.CONFIG_PATH):
        os.mkdir(f"{cf.CONFIG_PATH}/{folder}")
    os.rename(old_file_path, new_file_path)

def make_configs():
    """Create config dir/files if it doesn't exist"""

    if not os.path.isdir(cf.CONFIG_PATH):

        os.makedirs(cf.CONFIG_PATH)

    if os.listdir(cf.CONFIG_PATH) == []:

        os.makedirs(f"{cf.CONFIG_PATH}/naivenotes")
        with open(f"{cf.CONFIG_PATH}/naivenotes/exemple.md", "w") as f_out:
            with open("note_welcome.md", "r") as f_in:
                f_out.write(f_in.read())

    if not ".corbeille" in os.listdir(cf.CONFIG_PATH):

        os.mkdir(f"{cf.CONFIG_PATH}/.corbeille")


if __name__ == "__main__":

    app = QApplication(sys.argv)
    ex = NaiveNoteApp()
    sys.exit(app.exec_())
