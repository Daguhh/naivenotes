#!/usr/bin/env python3

import os
import configparser

####### Config paths ######################
HOME = os.getenv("HOME")
CONFIG_PATH = f"{HOME}/.config/naivenotes"
# APP_SAVE_FILE = f"{CONFIG_PATH}/app_save.json"
# USER_CONFIG = f"{CONFIG_PATH}/config.ini"
# DESKTOP_FILES_PATH = f"{CONFIG_PATH}/Apps"

######## Examples Files ################
# APP_TAB_EXAMPLE = "/usr/share/naifmenu/example/tab"
# APP_EXAMPLE = "/usr/share/naifmenu/example/app"
#
####### User define parameters #############
# CONFIG = configparser.ConfigParser()
# CONFIG.read(USER_CONFIG)
#
####### Icon paths #########################
# ICON_PATHS = [f'{HOME}/.local/share/icons', '/usr/share/icons']
# ICON_SIZES = [512, 310, 256, 192, 150, 128, 96, 72, 64, 48, 44, 42, 36, 32, 24, 22, 16, 8]
# ICON_DEFAULT = "/usr/share/naifmenu/example/app/empty.png"
#
####### App names #########################
# MENU_NAME = "naifmenu"
# MENU_TITLE = "NaïfMenu"
#
###### I3 related parameters ###############
# I3_RIGHT = "right"
# I3_LEFT = "left"
#
###### Default user parameters ############
# DEFAULT_CONF_INI = """
# [Icon]
# x = 150
# y = 60
# theme = hicolor
#
# [Themes]
# hicolor = hicolor, oxygen/base, Moka, Faba
# moka = Moka, oxygen/base, hicolor, Faba
# oxygen = oxygen/base, hicolor
# faba = Faba, hicolor
#
# [Window]
# x = 723
# y = 467
#
# [Options]
# dualpanel = False
# autoclose = True
# """
