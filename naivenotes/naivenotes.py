#!/usr/bin/env python3

import sys

from PyQt5.QtWidgets import QApplication

from main import NaiveNoteApp


if __name__ == "__main__":

    app = QApplication(sys.argv)
    ex = NaiveNoteApp()
    sys.exit(app.exec_())
