#!/usr/bin/env python3

from renders.md2html import MarkdownRender
from renders.rst2html import RstRender


class Converter:
    def __init__(self):

        # self._render = 'md'

        self._renders = {
            'md' : MarkdownRender(),
            'rst' : RstRender(),
        }

        self.setrender("md")

    def setrender(self, extention="md"):

        # self._render = 'md'
        if extention == "md":
            self.render = self._renders['md']
        elif extention == "rst":
            self.render = self._renders['rst']

    def convert(self, text):

        return self.render.convert(text)
