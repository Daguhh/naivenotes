#!/usr/bin/env python3

LIST_QSS = """
    QFrame {
        background: rgb(43,48,59);
        background-color: rgb(43,48,59);
        color: white;
        border: none;
        padding: 0px;
        margin: 0px;
        margin-left: 0px;
        font-size: 12px;
    }
    QListWidget {
        background: rgb(43,48,59);
        background-color: rgb(43,48,59);
        color: white;
        border: none;
        padding: 0px;
        margin: 0px;
        margin-left: 0px;
        font-size: 12px;
    }
    QListWidget::item:selected:active {
        background-color: rgb(0,0,0);
        font: bold 14px;
    }
"""

MAIN_WINDOW_QSS = """
    QListWidget {
        border: none;
        padding: 0px;
        margin: 0px;
        background: rgb(43,48,59);
        background-color: rgb(43,48,59);
        padding: 0px;
    }
"""

Text_Edit_QSS = """
    QPlainTextEdit {
        background: rgb(255,255,255);
    }
"""
