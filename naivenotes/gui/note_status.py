import base64

from PyQt5.QtCore import Qt, QObject, pyqtSignal, QByteArray
from PyQt5.QtGui import QIcon, QPixmap
from PyQt5.QtWidgets import QFrame, QPushButton, QLineEdit, QHBoxLayout, QVBoxLayout

from . import icon


class NoteStatus(QFrame):
    def __init__(self):
        super().__init__()
        self.setMaximumHeight(35)

        hbox = QHBoxLayout()
        self.detachBtn = QPushButton()
        self.detachBtn.setIcon(iconFromBase64(icon.ICON_POPUP))
        self.detachBtn.setFixedSize(30, 30)
        self.detachBtn.setToolTip("Place la zone d'édition dans une fenêtre flottante")
        self.nameEdit = NameEdit()
        self.nameEdit.setPlaceholderText("New note")
        self.toggleviewBtn = QPushButton()
        self.toggleviewBtn.setIcon(iconFromBase64(icon.ICON_SPLIT))
        self.toggleviewBtn.setFixedSize(30, 30)
        self.toggleviewBtn.setToolTip(
            "Bascule entre vue édition, rendu et vue partagée"
        )
        self.propertiesBtn = QPushButton()
        self.propertiesBtn.setIcon(iconFromBase64(icon.ICON_PARAMETERS))
        self.propertiesBtn.setFixedSize(30, 30)
        self.propertiesBtn.setToolTip("Afficher et Editer les propriétés du documents")
        hbox.addWidget(self.detachBtn)
        hbox.addWidget(self.nameEdit)
        hbox.addWidget(self.toggleviewBtn)
        hbox.addWidget(self.propertiesBtn)
        self.setLayout(hbox)

    def setname(self, name):
        self.nameEdit.setText(name)


class NameEdit(QLineEdit):
    def __init__(self):

        super().__init__()
        self.sig = MouseClickSignal()

    def mousePressEvent(self, event):

        if event.button() == Qt.LeftButton:
            self.sig.enterEditionEvent.emit()


def iconFromBase64(base64):
    """Read menu icon images from base64 string"""

    pixmap = QPixmap()
    pixmap.loadFromData(QByteArray.fromBase64(base64))
    icon = QIcon(pixmap)
    return icon


class MouseClickSignal(QObject):

    enterEditionEvent = pyqtSignal()
