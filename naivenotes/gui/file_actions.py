#!/uusr/bin/env python3

from PyQt5.QtCore import Qt, QObject, pyqtSignal
from PyQt5.QtWidgets import QPushButton, QLineEdit


class CreateFile(QPushButton):
    def __init__(self):
        super().__init__("+")
        self.setFixedSize(30, 30)


class DeleteFile(QPushButton):
    def __init__(self):
        super().__init__("-")
        self.setFixedSize(30, 30)


class SearchFiles(QLineEdit):
    def __init__(self):
        super().__init__()
        self.setPlaceholderText("Search for a note")
        self.sig = MouseClickSignal()

    def mousePressEvent(self, event):

        if event.button() == Qt.LeftButton:
            self.sig.enterEditionEvent.emit()


class MouseClickSignal(QObject):

    enterEditionEvent = pyqtSignal()
