#!/usr/bin/env python3


import sys, os, glob

# from PyQt5.QtCore import QString
from PyQt5.QtCore import Qt, QByteArray, pyqtSignal, QObject, QTimer
from PyQt5.QtGui import QPixmap, QIcon, QFont, QImage
from PyQt5.QtWidgets import (
    QHBoxLayout,
    QVBoxLayout,
    QFrame,
    QSplitter,
)

from .note_editor import NoteEditor
from .note_web_preview import NoteWebPreview
from .note_status import NoteStatus

from .file_tree import FilesTreeWidget
from .file_list import FilesListWidget
from .file_actions import SearchFiles, CreateFile, DeleteFile
from .popup_editor import FreeEditor

from .qss import *


class MainWindow(QFrame):
    def __init__(self):

        super().__init__()
        self.title = "NaiveNotes"
        self.setWindowTitle(self.title)
        self.setGeometry(100, 100, 100, 100)
        # self.setStyleSheet(MAIN_WINDOW_QSS)

        self.setFrameShape(QFrame.NoFrame)

        hbox = QHBoxLayout()

        self.splitter = QSplitter(Qt.Horizontal)
        self.files = FileFrame()
        self.note = NoteFrame()


        self.splitter.addWidget(self.files)
        self.splitter.addWidget(self.note)
        self.splitter.setSizes([500, 3000])

        hbox.addWidget(self.splitter)
        self.setLayout(hbox)
        self.show()

        self.display_mod = loop_over(3)

        self.timer = QTimer(self)
        self.timer.setInterval(1000)

    def toggle_view(self):

        view = next(self.display_mod)
        print(view)

        if view == DUAL_PANEL_VIEW:
            self.note.editor.show()
            self.note.preview.show()
        elif view == EDITOR_VIEW:
            self.note.editor.show()
            self.note.preview.hide()
        elif view == WEB_VIEW:
            self.note.preview.show()
            self.note.editor.hide()


DUAL_PANEL_VIEW = 0
EDITOR_VIEW = 1
WEB_VIEW = 2


def loop_over(nb):
    """return a generator that loop from 0 to nb"""
    view = 0
    while 1:
        view = (view + 1) % nb
        yield view


class NoteFrame(QFrame):
    def __init__(self):

        super().__init__()

        vbox_note = QVBoxLayout()

        self.status = NoteStatus()

        self.splitter = QSplitter()

        self.editor = NoteEditor()
        self.preview = NoteWebPreview()
        self.preview.setMinimumWidth(200)

        self.splitter.addWidget(self.editor)
        self.splitter.addWidget(self.preview)
        self.splitter.setSizes([2000, 2000])

        vbox_note.addWidget(self.status)
        vbox_note.addWidget(self.splitter)

        self.setLayout(vbox_note)

        self.randomFrame = QFrame()
        self.randomFrame.setMaximumWidth(20)

        self.isEditorDetached = False

    def dtach_editor(self):

        if self.isEditorDetached:
            self.splitter.replaceWidget(0, self.editor)
            del self.popup_editor

        else:
            editor = self.splitter.replaceWidget(0, self.randomFrame)
            self.popup_editor = self.popout_frame(editor)

        self.isEditorDetached = not self.isEditorDetached

    def popout_frame(self, frame):

        return FreeEditor(frame)


class FileFrame(QFrame):
    def __init__(self):
        super().__init__()

        self.setFrameShape(QFrame.NoFrame)
        self.setMinimumWidth(150)
        self.setMaximumWidth(400)
        self.setStyleSheet(LIST_QSS)

        vbox_files = QVBoxLayout()

        self.search = SearchFiles()
        self.createBtn = CreateFile()
        self.delBtn = DeleteFile()

        frame_file_mgr = QFrame()
        hbox_file_mgr = QHBoxLayout()
        hbox_file_mgr.addWidget(self.search)
        hbox_file_mgr.addWidget(self.createBtn)
        hbox_file_mgr.addWidget(self.delBtn)
        self.treeWdg = FilesTreeWidget()
        self.listWdg = FilesListWidget()
        self.listWdg.hide()
        frame_file_mgr.setLayout(hbox_file_mgr)

        vbox_files.addWidget(frame_file_mgr)
        vbox_files.addWidget(self.treeWdg)
        vbox_files.addWidget(self.listWdg)

        self.view = self.treeWdg

        self.isTreeview = True

        self.setLayout(vbox_files)

    def toggle_search_mod(self):

        if self.isTreeview:
            self.treeWdg.hide()
            self.listWdg.show()
            self.listWdg.update()
            self.view = self.listWdg
        else:
            self.treeWdg.show()
            self.listWdg.hide()
            self.treeWdg.update()
            self.view = self.treeWdg

        self.isTreeview = not self.isTreeview

    def search_file(self, keyword):

        self.view.update()
        matches = self.listWdg.findItems(keyword, Qt.MatchContains)
        file_names = [m.text() for m in matches]
        self.listWdg.clear()
        self.listWdg.addItems(file_names)
