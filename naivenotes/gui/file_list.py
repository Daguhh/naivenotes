import glob

from PyQt5.QtWidgets import QListWidget

from .qss import *
import config as cf


class FilesListWidget(QListWidget):
    def __init__(self):
        super().__init__()
        self.setStyleSheet(LIST_QSS)

        self.selected = SelectedListItem(self)

    def update(self):
        self.clear()

        file_paths = list(glob.iglob(f"{cf.CONFIG_PATH}/*/*"))

        file_names = [path.split("/")[-1] for path in file_paths]
        file_folders = [path.split("/")[-2] for path in file_paths]
        dupls = [file_names.count(name) for name in file_names]
        display_names = [
            name if d == 1 else f"{name} ({folder})"
            for name, folder, d in zip(file_names, file_folders, dupls)
        ]
        self.dct = {dn: f for dn, f in zip(display_names, file_paths)}
        self.addItems(display_names)

    def ItemSelectionChanged(self):
        print("item changed")


class SelectedListItem:
    def __init__(self, outter):
        self.outter = outter

    @property
    def item(self):
        return self.outter.currentItem()

    @property
    def name(self):
        return self.item.text()

    @name.setter
    def name(self, name):
        self.item.setText(name)

    @property
    def type(self):
        return "file"

    @property
    def folder(self):
        return self.path.split("/")[-2]

    @property
    def path(self):
        filename = self.name
        file_path = self.outter.dct[filename]
        return file_path
