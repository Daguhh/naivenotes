#!/usr/bin/env python3

from PyQt5.QtWidgets import QWidget, QHBoxLayout


class FreeEditor(QWidget):
    def __init__(self, editor):

        super().__init__()

        self.hbox = QHBoxLayout()
        self.hbox.addWidget(editor)
        self.setLayout(self.hbox)
        self.show()
