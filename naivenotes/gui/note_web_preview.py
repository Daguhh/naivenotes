#!/usr/bin/env python3

from PyQt5.QtWebEngineWidgets import QWebEngineView


class NoteWebPreview(QWebEngineView):
    def __init__(self):

        super().__init__()

    def follow(self, editor):

        self.followed_editor = editor
        self.followed_editor.textChanged.connect(self.update_preview)

    def update_preview(self):

        html = self.followed_editor.gethtml()
        self.setHtml(html)
