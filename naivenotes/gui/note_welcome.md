NaïveNotes
============

Un très simple (voire naïf) gestionnaire de notes au format markdown. Réalisé en python avec les bibliothèques **PyQt5** et **python-markdown**.

### Fonctionnalités :

* Notes stockées dans un dossier unique sous forme de fichiers markdown
* Basculez entre les vues *édition/rendu html/vue scindée*
* Détachez en un popup la zone d'édition pour prendre des notes sans perdre de vue votre travail.
* Sauvegarde automatique des notes

### Usage:

```
cd naivenotes/
```

```
python3 -m pip install -r requirements
```

```
python3 main.py
```




