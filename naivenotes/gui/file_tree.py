from PyQt5.QtCore import Qt, QObject
from PyQt5.QtWidgets import QTreeWidget, QTreeWidgetItem

from .qss import *
from config import *


class FilesTreeWidget(QTreeWidget):
    def __init__(self):
        super().__init__()
        self.setHeaderHidden(True)
        self.setStyleSheet(LIST_QSS)

        self.selected = SelectedTreeItem(self)
        self.tree = {}

    def addFolder(self, folder):

        if not folder in self.tree.keys():
            self.tree[folder] = FolderItem(self, folder)

    def addFile(self, file, folder):

        self.addFolder(folder)
        self.tree[folder].addFile(file)
        self.select_new_item(folder)

    def addFile2CurFolder(self, name):

        self.tree[self.selected.folder].addFile(name)
        self.select_new_item(self.selected.folder)

    def rmFolder(self, folder):

        self.removeItemWidget(self.tree[folder].item, 0)
        index = self.indexOfTopLevelItem(self.tree[folder].item)
        self.takeTopLevelItem(index)
        del self.tree[folder]

    def rmFile(self):

        file_item = self.selected.item
        folder = self.selected.folder

        index = self.tree[folder].item.indexOfChild(file_item)
        self.tree[folder].item.takeChild(index)

    def mvFile(self, name, folder):

        self.rmFile()
        self.addFile(name, folder)

    def clear_all(self):
        self.clear()
        self.tree = {}

    def select_new_item(self, folder):
        self.setCurrentItem(self.tree[folder].children[-1].item)


#    def update(self, folder_struct):
#
#        self.tree = {}
#        self.clear()
#
#        next(folder_struct)
#
#        for folder, _, files in folder_struct:
#            self.tree[folder] = FolderItem(self, folder)
#            for file in files:
#                self.tree[folder].addFile(file)
#
#        for folder in os.listdir(CONFIG_PATH):
#            self.tree[folder] = FolderItem(self, folder)
#            for f in os.listdir(f"{CONFIG_PATH}/{folder}"):
#                self.tree[folder].addFile(f)


class FolderItem:
    def __init__(self, parent, name):
        self.name = name
        self.parent = parent
        self.item = QTreeWidgetItem(self.parent, [name])
        self.children = []

    def addFile(self, name):
        self.children.append(FileItem(self, name))

    def addItem(self, item):
        self.children.append(item)
        self.item.addChild(item)


class FileItem:
    def __init__(self, parent, name):
        self.name = name
        self.parent = parent
        self.item = QTreeWidgetItem(parent.item, [name])


class SelectedTreeItem:
    def __init__(self, outter):
        self.outter = outter

    @property
    def item(self):
        return self.outter.currentItem()

    @property
    def name(self):
        return self.item.text(0)

    @name.setter
    def name(self, name):
        self.item.setText(0, name)

    @property
    def type(self):
        if self.name in self.outter.tree.keys():
            return "folder"
        else:
            return "file"

    @property
    def folder(self):
        if self.type == "file":
            return self.item.parent().text(0)
        else:
            return self.name

    @property
    def path(self):
        filename = self.name
        foldername = self.item.parent().text(0)
        path = f"{CONFIG_PATH}/{foldername}/{filename}"
        return path
