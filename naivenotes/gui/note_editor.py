#!/usr/bin/env python3

from PyQt5.QtWidgets import QPlainTextEdit

from .qss import *

# from converter import Converter


class NoteEditor(QPlainTextEdit):
    def __init__(self):

        super().__init__()

        self.setDocumentTitle("NewDoc")
        self.setPlaceholderText("Entrez votre texte au format markdown ici")
        self.setUndoRedoEnabled(False)
        self.setStyleSheet(Text_Edit_QSS)

    def set_html_render(self, render):
        self.html_render = render

    def gettext(self):
        return self.toPlainText()

    def gethtml(self):
        return self.html_render.convert(self.gettext())

    def setText(self, text):
        self.setPlainText(text)


#    def load(self, filepath):
#        with open(filepath) as f:
#            text = f.read()
#        self.setPlainText(text)
