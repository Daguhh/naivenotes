#!/usr/bin/env python3

from distutils.core import setup

setup(
    name='Naivenotes',
    version='0.0-1',
    url='https://framagit.org/Daguhh/naivenotes',
    author = 'Daguhh',
    author_email = 'code.daguhh@zaclys.net',
    maintainer= 'Daguhh',
    maintainer_email = 'code.daguhh@zaclys.net',
    keywords = 'note-taking markdown',
    license='The MIT License (MIT)',
)

